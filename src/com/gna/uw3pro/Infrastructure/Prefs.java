package com.gna.uw3pro.Infrastructure;

public class Prefs {
	public static final String PREFS_NAME = "WWUPreferences";
	public static final String Hour = "hour";
	public static final String Minute = "minute";
	public static final String Second = "second";

	public static final String LicenceVerify = "isVerify";

}
