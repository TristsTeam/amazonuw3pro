package com.gna.uw3pro;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.uw3pro.Infrastructure.KeepScreenOnBaseClass;
import com.gna.uw3pro.Infrastructure.WWUCommon;

public class InfoActivity extends KeepScreenOnBaseClass implements
		OnClickListener {
	private static String REVMOB_ID = "519b995523dad9d8270001fb";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info);
		intializeViews();
		keepScreenOn();
    chartboostSessionStart();

	}

	private void intializeViews() {
		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		StateListDrawable emailBtnStates = new StateListDrawable();
		emailBtnStates.addState(new int[] { android.R.attr.state_pressed },
				this.getResources().getDrawable(R.drawable.emailbar_down));
		emailBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.emailbar_norm));

        StateListDrawable joinusBtnStates = new StateListDrawable();
        joinusBtnStates.addState(new int[] { android.R.attr.state_pressed },
                this.getResources().getDrawable(R.drawable.joinus_btn_down));
        joinusBtnStates.addState(new int[] {},
                this.getResources().getDrawable(R.drawable.joinus_btn_norm));

		TextView joinUs = (TextView) findViewById(R.id.joinUsTxt);
		TextView aboutWWU = (TextView) findViewById(R.id.aboutWWUTxt);
		TextView freeGame = (TextView) findViewById(R.id.freeGame);
		TextView instruction = (TextView) findViewById(R.id.instructionTxt);
		TextView share = (TextView) findViewById(R.id.shareTxt);
		TextView support = (TextView) findViewById(R.id.supportTxt);
		TextView workoutTips = (TextView) findViewById(R.id.workoutTips);
		ImageButton leftArrowClick = (ImageButton) findViewById(R.id.leftArrowClick);
        ImageView infoUrlClick=(ImageView)findViewById(R.id.infourl);
		ImageButton emailBtn = (ImageButton) findViewById(R.id.emailBtn);
		emailBtn.setBackgroundDrawable(emailBtnStates);
		emailBtn.setOnClickListener(this);

        joinUs.setBackgroundDrawable(joinusBtnStates);
		joinUs.setTypeface(koratakiRgTypeFace);
		aboutWWU.setTypeface(koratakiRgTypeFace);
		instruction.setTypeface(koratakiRgTypeFace);
		share.setTypeface(koratakiRgTypeFace);
		support.setTypeface(koratakiRgTypeFace);
		freeGame.setTypeface(koratakiRgTypeFace);
		workoutTips.setTypeface(koratakiRgTypeFace);

		joinUs.setOnClickListener(this);
		aboutWWU.setOnClickListener(this);
		freeGame.setOnClickListener(this);
		instruction.setOnClickListener(this);
		share.setOnClickListener(this);
		support.setOnClickListener(this);
		leftArrowClick.setOnClickListener(this);
		workoutTips.setOnClickListener(this);
        infoUrlClick.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		} else if (view.getId() == R.id.supportTxt) {
			supportEmail();
		} else if (view.getId() == R.id.shareTxt) {
			Intent intent = new Intent(this, ShareActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.joinUsTxt) {
			Intent intent=new Intent(this,JoinUsDialogActivity.class);
            intent.putExtra("isComingFromSecond",false);
            startActivity(intent);
		} else if (view.getId() == R.id.aboutWWUTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.instructionTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.workoutTips) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 3);
			startActivity(intent);
		} else if (view.getId() == R.id.emailBtn) {
			email();
		} else if (view.getId() == R.id.freeGame) {
            if (WWUCommon.getInstance(this).isNetworkAvailable())
                onChartBoostClick();
            else {
                Intent intent = new Intent(this, GenericDialogActivity.class);
                intent.putExtra("message",
                        "Internet connection is not available.");
                intent.putExtra("isQuit", false);
                startActivity(intent);
            }
		}else if(view.getId()==R.id.infourl)
        {
            Intent intent=new Intent(this,VideoPopUpActivity.class);
            startActivity(intent);
        }
	}

	private void supportEmail() {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "Support@UltimateFitnessApp.com" });
		i.putExtra(Intent.EXTRA_SUBJECT, "UWA Support Ticket");

		this.startActivity(Intent.createChooser(i, "Select application"));

	}

	public void email() {
		String url = "http://www.amazon.com/gp/mas/dl/android?p=com.gna.uw3pro";
		SpannableStringBuilder builder = new SpannableStringBuilder();
		builder.append("Check out this great workout app I am using -");
		int start = builder.length();
		builder.append(url);
		int end = builder.length();

		builder.setSpan(new URLSpan(url), start, end,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL, new String[] { "" });
		i.putExtra(Intent.EXTRA_SUBJECT, "I workout! :)");
		i.putExtra(Intent.EXTRA_TEXT, builder);
		startActivityForResult(Intent.createChooser(i, "Select application"), 1);

	}
    @Override
    protected void onStart() {
        super.onStart();

        cb.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        cb.onStop(this);
    }

    @Override
    public void onBackPressed() {
        if (cb.onBackPressed())
            // If a Chartboost view exists, close it and return
            return;
        else
            // If no Chartboost view exists, continue on as normal
            super.onBackPressed();
    }


}
