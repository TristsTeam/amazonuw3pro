package com.gna.uw3pro;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gna.uw3pro.Infrastructure.KeepScreenOnBaseClass;

/**
 * Created by rohitgarg on 6/3/13.
 */
public class JoinUsDialogActivity extends KeepScreenOnBaseClass implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.join_us_dialog);
        keepScreenOn();
        LinearLayout layoutClick=(LinearLayout)findViewById(R.id.tapHereBtn);
        layoutClick.setOnClickListener(this);

        ImageView leftBtn = (ImageView) findViewById(R.id.leftBtn);
        leftBtn.setOnClickListener(this);

        LinearLayout noThanksBtn=(LinearLayout)findViewById(R.id.noThanksBtn);
        noThanksBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()== R.id.tapHereBtn)
        {
            Intent myWebLink = new Intent(Intent.ACTION_VIEW);
            myWebLink
                    .setData(Uri
                            .parse("http://www.ultimatefitnessapp.com/join-us-at-ultimate-fitness-app-vip-insider/"));
            startActivity(myWebLink);
        }else if (view.getId() == R.id.leftBtn||view.getId()== R.id.noThanksBtn) {
            if(getIntent().getExtras().getBoolean("isComingFromSecond"))
            {
                Intent intent=new Intent(this,HomeActivity.class);
                startActivity(intent);
                this.finish();
            }
            else
            {
                this.finish();
            }
        }
    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        if(getIntent().getExtras().getBoolean("isComingFromSecond"))
        {
            Intent intent=new Intent(this,HomeActivity.class);
            startActivity(intent);
            this.finish();
        }
        else
        {
            this.finish();
        }
    }
}
