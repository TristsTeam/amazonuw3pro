package com.gna.uw3pro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.uw3pro.Infrastructure.KeepScreenOnBaseClass;

public class GenericInputDialogActivity extends KeepScreenOnBaseClass implements
		OnClickListener {
	EditText etInput;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.generic_input_dialog);
		keepScreenOn();

		ImageButton btnConfirm = (ImageButton) this
				.findViewById(R.id.btnConfirmInputDialog);
		ImageButton btnCancel = (ImageButton) this
				.findViewById(R.id.btnCancelInputDialog);
		etInput = (EditText) this.findViewById(R.id.etInput);

		btnConfirm.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		TextView tvDialogMessage = (TextView) findViewById(R.id.tvInputDialogMessage);
		if (getResources().getBoolean(R.bool.isTablet))
			tvDialogMessage.setTextSize(30);

	}

	@Override
	public void onClick(View view) {
		HideSoftKeypad();
		String inputText = etInput.getText().toString().trim();
		boolean dialogResult = false;
		if (view.getId() == R.id.btnConfirmInputDialog) {
			if (inputText.length() < 1) {
				etInput.setError("Required");
				return;
			} else {
				dialogResult = true;
			}
		} else {
			dialogResult = false;
		}
		Intent intent = new Intent();
		intent.putExtra("dialogresult", dialogResult);
		intent.putExtra("inputvalue", inputText);
		setResult(RESULT_OK, intent);
		finish();
	}

	private void HideSoftKeypad() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
}
