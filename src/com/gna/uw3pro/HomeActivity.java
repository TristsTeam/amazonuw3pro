package com.gna.uw3pro;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.gna.uw3pro.Infrastructure.CommonBaseClass;
import com.gna.uw3pro.Infrastructure.WWUCommon;

public class HomeActivity extends CommonBaseClass implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.home);
		intializeStates();
		intializeViews();
		keepScreenOn();
    chartboostSessionStart();


	}

	@Override
	protected void onStart() {
		super.onStart();

		cb.onStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();

		cb.onStop(this);
	}

	@Override
	public void onBackPressed() {
		if (cb.onBackPressed())
			// If a Chartboost view exists, close it and return
			return;
		else
			// If no Chartboost view exists, continue on as normal
			super.onBackPressed();
	}

	private void intializeViews() {

		StateListDrawable moreBtnStates = new StateListDrawable();
		moreBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_more_down));
		moreBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_more_norm));

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.maininfo_down));
		infoBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.maininfo_norm));

		StateListDrawable workoutAStates = new StateListDrawable();
		workoutAStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_01_down));
		workoutAStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_01_norm));

		StateListDrawable workoutBStates = new StateListDrawable();
		workoutBStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_02_down));
		workoutBStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_02_norm));

		StateListDrawable workoutCStates = new StateListDrawable();
		workoutCStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_03_down));
		workoutCStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_03_norm));

		StateListDrawable workoutDStates = new StateListDrawable();
		workoutDStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_04_down));
		workoutDStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_04_norm));

		StateListDrawable workoutEStates = new StateListDrawable();
		workoutEStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.mainworkout_05_down));
		workoutEStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.mainworkout_05_norm));

		StateListDrawable ufaBarStates = new StateListDrawable();
		ufaBarStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.ufabar_down));
		ufaBarStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.ufabar_norm));

		StateListDrawable takeABreakStates = new StateListDrawable();
		takeABreakStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_takeabreak_down));
		takeABreakStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_takeabreak_norm));

		StateListDrawable shareWebStates = new StateListDrawable();
		shareWebStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_web_down));
		shareWebStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_web_norm));

		StateListDrawable plusBtnStates = new StateListDrawable();
		plusBtnStates.addState(new int[] { android.R.attr.state_pressed },
				getResources().getDrawable(R.drawable.main_plus_down));
		plusBtnStates.addState(new int[] {},
				getResources().getDrawable(R.drawable.main_plus_norm));

		ImageView moreBtn = (ImageView) findViewById(R.id.moreBtn);
		moreBtn.setBackgroundDrawable(moreBtnStates);
		moreBtn.setOnClickListener(this);

		ImageButton plusBtn = (ImageButton) findViewById(R.id.plusBtn);
		plusBtn.setBackgroundDrawable(plusBtnStates);
		plusBtn.setOnClickListener(this);

		ImageButton shareWebBtn = (ImageButton) findViewById(R.id.shareWebBtn);
		shareWebBtn.setOnClickListener(this);
		shareWebBtn.setBackgroundDrawable(shareWebStates);

		ImageView infoBtn = (ImageView) findViewById(R.id.infoBtn);
		infoBtn.setBackgroundDrawable(infoBtnStates);
		infoBtn.setOnClickListener(this);

		ImageButton ufaBarBtn = (ImageButton) findViewById(R.id.ufaBarBtn);
		ufaBarBtn.setBackgroundDrawable(ufaBarStates);
		ufaBarBtn.setOnClickListener(this);

		ImageView takeABreak = (ImageView) findViewById(R.id.takeABreak);
		takeABreak.setBackgroundDrawable(takeABreakStates);
		takeABreak.setOnClickListener(this);

		emailButton.setOnClickListener(this);
		//musicButton.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		ImageButton workoutARightArrow = (ImageButton) findViewById(R.id.workoutARightArrow);
		ImageButton workoutBRightArrow = (ImageButton) findViewById(R.id.workoutBRightArrow);
		ImageButton workoutCRightArrow = (ImageButton) findViewById(R.id.workoutCRightArrow);
		ImageButton workoutDRightArrow = (ImageButton) findViewById(R.id.workoutDRightArrow);
		ImageButton workoutERightArrow = (ImageButton) findViewById(R.id.workoutERightArrow);
		workoutARightArrow.setOnClickListener(this);
		workoutBRightArrow.setOnClickListener(this);
		workoutCRightArrow.setOnClickListener(this);
		workoutDRightArrow.setOnClickListener(this);
		workoutERightArrow.setOnClickListener(this);

		ImageView workoutACLick = (ImageView) findViewById(R.id.workoutACLick);
		ImageView workoutBClick = (ImageView) findViewById(R.id.workoutBClick);
		ImageView workoutCClick = (ImageView) findViewById(R.id.workoutCClick);
		ImageView workoutDClick = (ImageView) findViewById(R.id.workoutDClick);
		ImageView workoutEClick = (ImageView) findViewById(R.id.workoutEClick);

		workoutACLick.setBackgroundDrawable(workoutAStates);
		workoutBClick.setBackgroundDrawable(workoutBStates);
		workoutCClick.setBackgroundDrawable(workoutCStates);
		workoutDClick.setBackgroundDrawable(workoutDStates);
		workoutEClick.setBackgroundDrawable(workoutEStates);

		workoutACLick.setOnClickListener(this);
		workoutBClick.setOnClickListener(this);
		workoutCClick.setOnClickListener(this);
		workoutDClick.setOnClickListener(this);
		workoutEClick.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.moreBtn || view.getId() == R.id.ufaBarBtn
				|| view.getId() == R.id.plusBtn) {
			Intent intent = new Intent(this, MoreActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, InfoActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.emailBarBtn) {
			email();
		} else if (view.getId() == R.id.shareWebBtn) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink.setData(Uri.parse("http://www.ultimatefitnessapp.com/"));
			startActivity(myWebLink);
		} 
//		else if (view.getId() == R.id.musicBarBtn) {
//			try {
//				Intent intent = new Intent(android.content.Intent.ACTION_MAIN);
//				intent.addCategory("android.intent.category.APP_MUSIC");
//				startActivity(intent);
//			} catch (ActivityNotFoundException e) {
//				Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
//				startActivity(intent);
//			}
//		} 
		else if (view.getId() == R.id.workoutACLick
				|| view.getId() == R.id.workoutARightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.workoutBClick
				|| view.getId() == R.id.workoutBRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.workoutCClick
				|| view.getId() == R.id.workoutCRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 3);
			startActivity(intent);
		} else if (view.getId() == R.id.workoutDClick
				|| view.getId() == R.id.workoutDRightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 4);
			startActivity(intent);
		} else if (view.getId() == R.id.workoutEClick
				|| view.getId() == R.id.workoutERightArrow) {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", 5);
			startActivity(intent);
		} else if (view.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.takeABreak) {
			if (WWUCommon.getInstance(this).isNetworkAvailable())
				onChartBoostClick();
			else {
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message",
						"Internet connection is not available.");
				intent.putExtra("isQuit", false);
				startActivity(intent);
			}
		}
	}


}
